#include <node.h>

extern "C" {
  #include <libavformat/avformat.h>
}
#define _STR(x) String::NewFromUtf8(isolate, x)
namespace mediainfo {

  using v8::FunctionCallbackInfo;
  using v8::Isolate;
  using v8::Local;
  using v8::Object;
  using v8::String;
  using v8::Integer;
  using v8::Number;
  using v8::Value;
  using v8::Exception;
  using v8::Array;
  
  /**
   * calculateTime
   * convert a timestamp to seconds
   */
  double calculateTime(int64_t ts, AVRational* time_base) {
    return ts * av_q2d(*time_base);
  }
  
  /**
   * av_dictionary_to_js
   * Convert a av-dictionary to a js object
   */
  Local<Object> av_dictionary_to_js(Isolate* isolate, AVDictionary* dict) {
    Local<Object> obj = Object::New(isolate);
    AVDictionaryEntry *entry = NULL;
    
    while((entry = av_dict_get(dict, "", entry, AV_DICT_IGNORE_SUFFIX))) {
      obj->Set(String::NewFromUtf8(isolate, entry->key), String::NewFromUtf8(isolate, entry->value));
    }
    
    return obj;
  }
  
  /**
   * chapter_to_js
   * Convert a AVChapter to a js object
   */
  Local<Object> chapter_to_js(Isolate* isolate, AVChapter* chapter) {
    Local<Object> obj = Object::New(isolate);
    
    Local<Object> timebase = Object::New(isolate);
    timebase->Set(_STR("num"), Integer::New(isolate, chapter->time_base.num));
    timebase->Set(_STR("den"), Integer::New(isolate, chapter->time_base.den));
    
    obj->Set(_STR("start"), Number::New(isolate, chapter->start));
    obj->Set(_STR("end"), Number::New(isolate, chapter->end));
    obj->Set(_STR("start_sec"), Number::New(isolate, calculateTime(chapter->start, &chapter->time_base)));
    obj->Set(_STR("end_sec"), Number::New(isolate, calculateTime(chapter->end, &chapter->time_base)));
    obj->Set(_STR("timebase"), timebase);
    obj->Set(_STR("metadata"), av_dictionary_to_js(isolate, chapter->metadata));
    return obj;
  }
  
  /**
   * file_info
   * Extract info about a media file, using avformat
   */
  void file_info(const FunctionCallbackInfo<Value>& args) {
    Isolate* isolate = args.GetIsolate();
    AVFormatContext *pFmtCtx = NULL;

    if(args.Length() < 1) {
      isolate->ThrowException(Exception::TypeError(
        String::NewFromUtf8(isolate, "Wrong number of arguments")));
      return;
    }

    if(!args[0]->IsString()) {
      isolate->ThrowException(Exception::TypeError(
        String::NewFromUtf8(isolate, "Wrong argument, must be string")));
      return;
    }

    String::Utf8Value filename(args[0]);
    // Open file
    av_register_all();
    if(avformat_open_input(&pFmtCtx, *filename, NULL, NULL) < 0) {
      isolate->ThrowException(
        Exception::Error(String::NewFromUtf8(isolate, "Unable to open file")));
      return;
    }
    
    Local<Object> obj = Object::New(isolate);

    // Read chapter info
    
    Local<Array> js_chapters = Array::New(isolate, pFmtCtx->nb_chapters);
    AVChapter* chapter = NULL;
    for(unsigned int i=0; i<pFmtCtx->nb_chapters; ++i) {
      chapter = pFmtCtx->chapters[i];
      js_chapters->Set(i, chapter_to_js(isolate, chapter));
    }
    
    obj->Set(_STR("chapters"), js_chapters);
    obj->Set(_STR("metadata"), av_dictionary_to_js(isolate, pFmtCtx->metadata));

    // Cleanup
    avformat_close_input(&pFmtCtx);
    args.GetReturnValue().Set(obj);
  }


  void init(Local<Object> exports) {
    NODE_SET_METHOD(exports, "file_info", file_info);
  }
  NODE_MODULE(mediainfo, init)
}
